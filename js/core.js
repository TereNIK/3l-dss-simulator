let nodes;
let edges;
let options;

let icon_server = '\uf233';
let icon_storage = '\uf0a0';
let icon_cloud = '\uf0c2';

let nodes_start = [{
        id: 1,
        label: 'Level 3',
        group: 'lvl3',
        level: 0
    }, {
        id: 2,
        label: 'Level 2-1',
        group: 'lvl2',
        level: 1
    }, {
        id: 3,
        label: 'Level 2-2',
        group: 'lvl2',
        level: 1
    }, {
        id: 4,
        label: 'Level 2-1-1',
        group: 'lvl1',
        level: 2
    }, {
        id: 5,
        label: 'Level 2-2-1',
        group: 'lvl1',
        level: 2
    }, {
        id: 6,
        label: 'Level 2-2-2',
        group: 'lvl1',
        level: 2
    }, {
        id: 7,
        label: 'Storage for level 2-1-1',
        group: 'storage',
        level: 3
    }, {
        id: 8,
        label: 'Storage for level 2-2-1',
        group: 'storage',
        level: 3
    }, {
        id: 9,
        label: 'Storage for level 2-2-2',
        group: 'storage',
        level: 3
    }
];
let edges_start = [{
    from: 1,
    to: 2,
    arrows: 'from'
}, {
    from: 1,
    to: 3,
    arrows: 'from'
}, {
    from: 2,
    to: 4,
    arrows: 'from'
}, {
    from: 3,
    to: 5,
    arrows: 'from'
}, {
    from: 3,
    to: 6,
    arrows: 'from'
}, {
    from: 4,
    to: 7,
    dashes: true
}, {
    from: 5,
    to: 8,
    dashes: true
}, {
    from: 6,
    to: 9,
    dashes: true
}];

let options_start = {
    nodes: {
        shape: 'icon',
        icon: {
            face: 'FontAwesome',
            code: icon_server,
            size: 50,
            color: '#2B7CE9'
        }
    },
    layout: {
        hierarchical: {
            direction: 'UD',
            sortMethod: 'directed'
        }
    },
    groups: {
        lvl3: {
            icon: {
                color: '#e9332b'
            }
        },
        lvl2: {
            icon: {
                color: '#4fe92b'
            }
        },
        lvl1: {
            icon: {
                color: '#2B7CE9'
            }
        },
        storage: {
            icon: {
                face: 'FontAwesome',
                code: icon_storage,
            }
        }
    },
    interaction: {
        dragNodes: false,
        dragView: false
    },
    edges: {
        arrows: {
            from: {
                scaleFactor: .9
            }
        }
    }
};

// create a network
let container = document.getElementById('main-diagram');


let form_mode = document.querySelector('#mode');
form_mode.onchange = changeModeHandler;

let form_level = document.querySelector('#level');
form_level.onchange = changeLevelHandler;


function changeModeHandler(event) {
    changeLevelHandler(event);

    if (document.getElementById('mode-1').checked) {
        options.nodes.icon.code = icon_server;
    } else {
        options.nodes.icon.code = icon_cloud;
        nodes.remove(7);
        nodes.remove(8);
        nodes.remove(9);
    }


    let data = {
        nodes: nodes,
        edges: edges
    };

    network = new vis.Network(container, data, options);
}


function main() {
    options = options_start;

    // start init array of Nodes and Edges
    nodes = new vis.DataSet(nodes_start);
    edges = new vis.DataSet(edges_start);

    // provide the data in the vis format
    let data = {
        nodes: nodes,
        edges: edges
    };


    // initialize your network!
    network = new vis.Network(container, data, options);
}


function changeLevelHandler(event) {
    // start init array of Nodes and Edges
    nodes = new vis.DataSet(nodes_start);
    edges = new vis.DataSet(edges_start);

    if (document.getElementById('mode-2').checked) {
        nodes.remove(7);
        nodes.remove(8);
        nodes.remove(9);
    }

    // If the checkbox is not checked, delete nodes
    if (document.getElementById("checkbox-level-3").checked == false) {
        nodes.remove(1);
    };

    if (document.getElementById("checkbox-level-2").checked == false) {
        nodes.remove(2);
        nodes.remove(3);
        edges.add([{
            from: 1,
            to: 4,
            arrows: 'from'
        }, {
            from: 1,
            to: 5,
            arrows: 'from'
        }, {
            from: 1,
            to: 6,
            arrows: 'from'
        }]);
    };

    if (document.getElementById("checkbox-level-1").checked == false) {
        nodes.remove(4);
        nodes.remove(5);
        nodes.remove(6);

        edges.add([{
            from: 2,
            to: 7,
            dashes: true
        }, {
            from: 3,
            to: 8,
            dashes: true
        }, {
            from: 3,
            to: 9,
            dashes: true
        }]);
    };

    if (document.getElementById('mode-2').checked == false &&
        document.getElementById("checkbox-level-1").checked == false &&
        document.getElementById("checkbox-level-2").checked == false) {
        edges.add([{
            from: 1,
            to: 7,
            dashes: true
        }, {
            from: 1,
            to: 8,
            dashes: true
        }, {
            from: 1,
            to: 9,
            dashes: true
        }]);
    };


    // provide the data in the vis format
    let data = {
        nodes: nodes,
        edges: edges
    };
    network = new vis.Network(container, data, options);
}
